CREATE TABLE `orders` (
    `id` BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `account` VARCHAR(36) NOT NULL,
    `created_at` BIGINT UNSIGNED NOT NULL,
    `is_webshop_purchase` BOOLEAN NOT NULL,
    `shipment_accepted_at` BIGINT UNSIGNED,
    `shipped_at` BIGINT UNSIGNED
);

CREATE TABLE `ordered_products` (
    `id` BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `product_id` BIGINT UNSIGNED NOT NULL,
    `order_id` BIGINT UNSIGNED NOT NULL,
    `amount` INT UNSIGNED NOT NULL,
    `unit_price` INT UNSIGNED NOT NULL,

    FOREIGN KEY (`order_id`) REFERENCES `orders`(`id`),
    INDEX(`amount`)
);