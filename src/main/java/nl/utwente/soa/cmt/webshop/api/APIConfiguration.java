package nl.utwente.soa.cmt.webshop.api;

import nl.utwente.soa.cmt.webshop.api.inventory.InventoryAPI;
import nl.utwente.soa.cmt.webshop.api.token.TokenAPI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Configuration
public class APIConfiguration {

    @Value("${tokenService.baseUrl}")
    private String tokenServiceBaseUrl;

    @Value("${inventoryService.baseUrl}")
    private String inventoryServiceBaseUrl;

    @Bean
    public TokenAPI tokenAPI() {
        return new Retrofit
            .Builder()
            .baseUrl(tokenServiceBaseUrl)
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
            .create(TokenAPI.class);
    }

    @Bean
    public InventoryAPI inventoryAPI() {
        return new Retrofit
            .Builder()
            .baseUrl(inventoryServiceBaseUrl)
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
            .create(InventoryAPI.class);
    }
}
