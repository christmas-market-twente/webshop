package nl.utwente.soa.cmt.webshop.api.inventory;

import nl.utwente.soa.cmt.webshop.api.APIResponse;
import nl.utwente.soa.cmt.webshop.api.inventory.request.DestockRequestDto;
import nl.utwente.soa.cmt.webshop.api.inventory.response.PresaleProductResponseDto;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface InventoryAPI {

    @GET("products/presale")
    Call<APIResponse<List<PresaleProductResponseDto>>> getPresale(@Query("ids") Long[] ids);

    @PUT("products/stock/remove")
    Call<Void> removeStock(@Body DestockRequestDto o);

    @PUT("products/stock/revert")
    Call<Void> revertStock(@Body DestockRequestDto o);
}
