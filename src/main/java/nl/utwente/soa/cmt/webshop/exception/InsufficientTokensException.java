package nl.utwente.soa.cmt.webshop.exception;

import org.springframework.http.HttpStatus;

public class InsufficientTokensException extends TokenServiceException {

    public InsufficientTokensException() {
        super(HttpStatus.CONFLICT, "Insufficient tokens");
    }
}
