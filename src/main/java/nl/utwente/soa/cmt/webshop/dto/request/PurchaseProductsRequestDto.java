package nl.utwente.soa.cmt.webshop.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseProductsRequestDto {

    @NotNull
    private Boolean isWebshopPurchase;

    @NotNull
    @Size(min = 1)
    @Valid
    private List<OrderedProductRequestDto> products;
}
