package nl.utwente.soa.cmt.webshop.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import nl.utwente.soa.cmt.webshop.dto.request.PurchaseProductsRequestDto;
import nl.utwente.soa.cmt.webshop.dto.response.OrderResponseDto;
import nl.utwente.soa.cmt.webshop.exception.InventoryServiceException;
import nl.utwente.soa.cmt.webshop.exception.TokenServiceException;
import nl.utwente.soa.cmt.webshop.response.ErrorResponse;
import nl.utwente.soa.cmt.webshop.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Tag(name = "Purchase", description = "The Purchase API")
public class PurchaseController {

    @Autowired
    private PurchaseService purchaseService;

    @Operation(summary = "Make a purchase", description = "Creates a purchase order", tags = {"Purchase"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "400", description = "Invalid request body",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
        @ApiResponse(responseCode = "404", description = "Product not found",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
        @ApiResponse(responseCode = "409", description = "Insufficient stock or insufficient tokens",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
        @ApiResponse(responseCode = "503", description = "Token service or inventory service is unavailable",
        content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @PostMapping("/purchase")
    public OrderResponseDto purchaseProducts(@Parameter(description = "A list of the products and amounts to be bought, and whether it is purchased on the webshop.")
                                                @Valid @RequestBody PurchaseProductsRequestDto purchaseProductsRequestDto,
                                             @Parameter(hidden = true)
                                                @RequestHeader("X-User-UUID") String userUUID) throws TokenServiceException, InventoryServiceException {
        return purchaseService.purchaseProducts(purchaseProductsRequestDto, userUUID);
    }
}
