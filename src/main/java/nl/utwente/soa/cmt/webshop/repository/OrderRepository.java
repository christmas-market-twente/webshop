package nl.utwente.soa.cmt.webshop.repository;

import nl.utwente.soa.cmt.webshop.model.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends CrudRepository<Order, Long> {

    List<Order> findAllByAccountOrderByCreatedAtDesc(String account);

    Optional<Order> findByIdAndAccount(Long id, String account);
}
