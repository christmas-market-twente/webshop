package nl.utwente.soa.cmt.webshop.queue.dto.respone;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShipStatusResponseDto {

    private Long orderId;

    private Long timestamp;

    private String status;
}
