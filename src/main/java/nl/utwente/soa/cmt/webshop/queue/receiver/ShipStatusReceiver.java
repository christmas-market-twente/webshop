package nl.utwente.soa.cmt.webshop.queue.receiver;

import nl.utwente.soa.cmt.webshop.model.Order;
import nl.utwente.soa.cmt.webshop.queue.dto.respone.ShipStatusResponseDto;
import nl.utwente.soa.cmt.webshop.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ShipStatusReceiver {

    @Autowired
    private OrderRepository orderRepository;

    @JmsListener(destination = "${shippingService.queue.order}")
    public void receiveMessage(ShipStatusResponseDto shipStatusResponseDto) {
        // TODO: make thread-safe
        Optional<Order> optOrder = orderRepository.findById(shipStatusResponseDto.getOrderId());
        if (optOrder.isEmpty()) {
            // Unknown order, so we ignore it.
            return;
        }

        Order order = optOrder.get();

        switch (shipStatusResponseDto.getStatus()) {
            case "Accepted":
                order.setShipmentAcceptedAt(shipStatusResponseDto.getTimestamp());
                break;
            case "Shipped":
                order.setShippedAt(shipStatusResponseDto.getTimestamp());
                break;
            default:
                // Unknown status, so we ignore it.
                return;
        }

        orderRepository.save(order);
    }
}
