package nl.utwente.soa.cmt.webshop.exception;

import org.springframework.http.HttpStatus;

public class TokenServiceUnavailableException extends TokenServiceException {

    public TokenServiceUnavailableException() {
        super(HttpStatus.SERVICE_UNAVAILABLE, "Token service unavailable");
    }
}
