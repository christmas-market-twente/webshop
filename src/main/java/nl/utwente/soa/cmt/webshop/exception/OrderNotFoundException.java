package nl.utwente.soa.cmt.webshop.exception;

import org.springframework.http.HttpStatus;

public class OrderNotFoundException extends ResponseException {

    public OrderNotFoundException() {
        super(HttpStatus.NOT_FOUND, "Order not found");
    }
}
