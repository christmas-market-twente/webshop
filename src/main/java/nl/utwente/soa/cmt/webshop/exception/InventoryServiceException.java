package nl.utwente.soa.cmt.webshop.exception;

import org.springframework.http.HttpStatus;

public abstract class InventoryServiceException extends ResponseException {

    public InventoryServiceException(HttpStatus status, String message) {
        super(status, "Inventory service error: " + message);
    }

    public InventoryServiceException(HttpStatus status) {
        super(status, "Inventory service error");
    }
}
