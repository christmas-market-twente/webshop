package nl.utwente.soa.cmt.webshop.queue.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ProductShippingRequestDto {

    @NotNull
    private Long productId;

    @NotNull
    @Min(1)
    private Integer amount;

    public ProductShippingRequestDto(Long productId, Integer amount) {
        this.productId = productId;
        this.amount = amount;
    }

    public ProductShippingRequestDto() {}
}
