package nl.utwente.soa.cmt.webshop.exception;

import org.springframework.http.HttpStatus;

public class InsufficientStockException extends InventoryServiceException {

    public InsufficientStockException() {
        super(HttpStatus.CONFLICT, "Insufficient stock");
    }
}
