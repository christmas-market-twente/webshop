package nl.utwente.soa.cmt.webshop.api.token;

import nl.utwente.soa.cmt.webshop.api.APIResponse;
import nl.utwente.soa.cmt.webshop.api.token.request.TransferTokensRequestDto;
import nl.utwente.soa.cmt.webshop.api.token.response.BalanceResponseDto;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface TokenAPI {

    @GET("tokens/balance")
    Call<APIResponse<BalanceResponseDto>> getBalance(@Header("X-User-UUID") String userUUID);

    @POST("tokens/transfer")
    Call<Void> transferTokens(@Header("X-User-UUID") String userUUID, @Body TransferTokensRequestDto o);
}
