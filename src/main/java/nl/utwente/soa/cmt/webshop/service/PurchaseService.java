package nl.utwente.soa.cmt.webshop.service;

import nl.utwente.soa.cmt.webshop.api.APIResponse;
import nl.utwente.soa.cmt.webshop.api.token.response.BalanceResponseDto;
import nl.utwente.soa.cmt.webshop.exception.*;
import nl.utwente.soa.cmt.webshop.queue.dto.request.ProductShippingRequestDto;
import nl.utwente.soa.cmt.webshop.queue.dto.request.ShippingRequestDto;
import nl.utwente.soa.cmt.webshop.repository.OrderRepository;
import nl.utwente.soa.cmt.webshop.api.inventory.InventoryAPI;
import nl.utwente.soa.cmt.webshop.api.inventory.request.DestockRequestDto;
import nl.utwente.soa.cmt.webshop.api.inventory.request.ProductDestockRequestDto;
import nl.utwente.soa.cmt.webshop.api.inventory.response.PresaleProductResponseDto;
import nl.utwente.soa.cmt.webshop.api.token.TokenAPI;
import nl.utwente.soa.cmt.webshop.api.token.request.TransactionRequestDto;
import nl.utwente.soa.cmt.webshop.api.token.request.TransferTokensRequestDto;
import nl.utwente.soa.cmt.webshop.dto.request.OrderedProductRequestDto;
import nl.utwente.soa.cmt.webshop.dto.request.PurchaseProductsRequestDto;
import nl.utwente.soa.cmt.webshop.dto.response.OrderResponseDto;
import nl.utwente.soa.cmt.webshop.model.Order;
import nl.utwente.soa.cmt.webshop.model.OrderedProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PurchaseService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private TokenAPI tokenAPI;

    @Autowired
    private InventoryAPI inventoryAPI;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Value("${shippingService.queue.shipping}")
    private String shippingQueue;

    public OrderResponseDto purchaseProducts(PurchaseProductsRequestDto purchaseProductsRequestDto, String userUUID) throws InventoryServiceException, TokenServiceException {
        Map<Long, Integer> productAmounts = purchaseProductsRequestDto
            .getProducts()
            .stream()
            .collect(Collectors.toMap(OrderedProductRequestDto::getProductId, OrderedProductRequestDto::getAmount));

        // =========================== //
        // REQUEST PRODUCT INFORMATION //
        // =========================== //
        Long[] ids = purchaseProductsRequestDto
            .getProducts()
            .stream()
            .map(OrderedProductRequestDto::getProductId)
            .toArray(Long[]::new);

        List<PresaleProductResponseDto> productInfos;
        try {
            Response<APIResponse<List<PresaleProductResponseDto>>> productInfoResponse = inventoryAPI.getPresale(ids).execute();
            if (!productInfoResponse.isSuccessful() || !productInfoResponse.body().isSuccess()) {
                throw new ProductNotFoundException();
            }
            productInfos = productInfoResponse.body().getData();
        } catch (IOException e) {
            throw new InventoryServiceUnavailableException();
        }

        // ========================= //
        // CHECK IF SUFFICIENT STOCK //
        // ========================= //
        if (!hasSufficientStock(productAmounts, productInfos)) {
            throw new InsufficientStockException();
        }

        // ================================= //
        // CHECK IF SUFFICIENT TOKEN BALANCE //
        // ================================= //
        BalanceResponseDto balanceResponseDto;
        try {
            Response<APIResponse<BalanceResponseDto>> balanceResponse = tokenAPI.getBalance(userUUID).execute();
            if (!balanceResponse.isSuccessful() || !balanceResponse.body().isSuccess()) {
                throw new TokenServiceUnavailableException();
            }
            balanceResponseDto = balanceResponse.body().getData();
        } catch (IOException e) {
            throw new TokenServiceUnavailableException();
        }

        int totalTokenAmount = productInfos
            .stream()
            .reduce(0, (acc, curr) -> acc + productAmounts.get(curr.getId()) * curr.getPrice(), Integer::sum);

        if (balanceResponseDto.getBalance() < totalTokenAmount) {
            throw new InsufficientTokensException();
        }

        // ================ //
        // DESTOCK PRODUCTS //
        // ================ //
        DestockRequestDto destockRequestDto = new DestockRequestDto(
            purchaseProductsRequestDto
                .getProducts()
                .stream()
                .map(p -> new ProductDestockRequestDto(p.getProductId(), p.getAmount()))
                .collect(Collectors.toList())
        );

        try {
            Response<Void> removeStockResponse = inventoryAPI.removeStock(destockRequestDto).execute();
            if (!removeStockResponse.isSuccessful()) {
                throw new InsufficientStockException();
            }
        } catch (IOException e) {
            throw new InventoryServiceUnavailableException();
        }

        // =============== //
        // TRANSFER TOKENS //
        // =============== //
        TransferTokensRequestDto transferTokensRequestDto = new TransferTokensRequestDto(
            productInfos
                .stream()
                .collect(Collectors.groupingBy(PresaleProductResponseDto::getVendor,
                    Collectors.summingInt(p -> productAmounts.get(p.getId()) * p.getPrice())))
                .entrySet()
                .stream()
                .map(e -> new TransactionRequestDto(e.getKey(), e.getValue()))
                .collect(Collectors.toList())
        );

        try {
            Response<Void> transferTokensResponse = tokenAPI.transferTokens(userUUID, transferTokensRequestDto).execute();
            if (!transferTokensResponse.isSuccessful()) {
                revertDestock(destockRequestDto);
                throw new InsufficientTokensException();
            }
        } catch (IOException e) {
            revertDestock(destockRequestDto);
            throw new TokenServiceUnavailableException();
        }

        // ============ //
        // CREATE ORDER //
        // ============ //
        Order order = new Order();
        order.setAccount(userUUID);
        order.setCreatedAt(new Date().getTime() / 1000);
        order.setIsWebshopPurchase(purchaseProductsRequestDto.getIsWebshopPurchase());

        Set<OrderedProduct> orderedProducts = new HashSet<>();
        for (PresaleProductResponseDto productInfo : productInfos) {
            OrderedProduct orderedProduct = new OrderedProduct();
            orderedProduct.setProductId(productInfo.getId());
            orderedProduct.setAmount(productAmounts.get(productInfo.getId()));
            orderedProduct.setUnitPrice(productInfo.getPrice());
            orderedProduct.setOrder(order);
            orderedProducts.add(orderedProduct);
        }
        order.setProducts(orderedProducts);

        order = orderRepository.save(order);

        // ======================================================= //
        // SHIP ORDER IF THE PURCHASE WAS MADE THROUGH THE WEBSHOP //
        // ======================================================= //
        if (order.getIsWebshopPurchase()) {
            ShippingRequestDto shippingRequest = new ShippingRequestDto(
                order.getId(),
                new Date().getTime() / 1000,
                order.getProducts().stream().map(p -> new ProductShippingRequestDto(p.getProductId(), p.getAmount())).collect(Collectors.toList())
            );

            jmsTemplate.convertAndSend(shippingQueue, shippingRequest);
        }

        return OrderResponseDto.from(order);
    }

    private boolean hasSufficientStock(Map<Long, Integer> productAmounts, List<PresaleProductResponseDto> productInfos) {
        for (PresaleProductResponseDto productInfo : productInfos) {
            if (productInfo.getAmount() < productAmounts.get(productInfo.getId())) {
                return false;
            }
        }

        return true;
    }

    private void revertDestock(DestockRequestDto restockRequestDto) {
        try {
            inventoryAPI.revertStock(restockRequestDto).execute();
        } catch (IOException ignored) {
            // An email should be sent to the stall holders of the products to inform them about inconsistencies. Out of scope for now.
        }
    }
}
