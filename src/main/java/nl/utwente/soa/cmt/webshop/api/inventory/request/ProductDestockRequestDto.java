package nl.utwente.soa.cmt.webshop.api.inventory.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDestockRequestDto {

    private Long id;

    private Integer change;

    public ProductDestockRequestDto(Long id, Integer change) {
        this.id = id;
        this.change = change;
    }

    public ProductDestockRequestDto() {}
}
