package nl.utwente.soa.cmt.webshop.api.token.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionRequestDto {

    private String toAccount;

    private Integer amount;

    public TransactionRequestDto(String toAccount, Integer amount) {
        this.toAccount = toAccount;
        this.amount = amount;
    }

    public TransactionRequestDto() {}
}
