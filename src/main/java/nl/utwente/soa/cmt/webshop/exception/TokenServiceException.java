package nl.utwente.soa.cmt.webshop.exception;

import org.springframework.http.HttpStatus;

public abstract class TokenServiceException extends ResponseException {

    TokenServiceException(HttpStatus status, String message) {
        super(status, "Token service error: " + message);
    }

    TokenServiceException(HttpStatus status) {
        super(status, "Token service error");
    }
}
