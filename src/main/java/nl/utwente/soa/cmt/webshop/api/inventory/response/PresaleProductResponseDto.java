package nl.utwente.soa.cmt.webshop.api.inventory.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PresaleProductResponseDto {

    private Long id;

    private Integer amount;

    private Integer price;

    private String vendor;
}
