package nl.utwente.soa.cmt.webshop.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import nl.utwente.soa.cmt.webshop.dto.response.OrderResponseDto;
import nl.utwente.soa.cmt.webshop.exception.OrderNotFoundException;
import nl.utwente.soa.cmt.webshop.response.ErrorResponse;
import nl.utwente.soa.cmt.webshop.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
@Tag(name = "Order", description = "The Order API")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Operation(summary = "Get a list of all orders of a user.", description = "Returns a list of all orders.", tags = {"Order"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation")})
    @GetMapping("")
    public List<OrderResponseDto> getAllOrders(@Parameter(hidden = true) @RequestHeader("X-User-UUID") String userUUID) {
        return orderService.getAllOrders(userUUID);
    }

    @Operation(summary = "Find an oder of a user by its ID.", description = "Returns a single order.", tags = {"Order"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "404", description = "No product was found with the specified ID or the order does not belong to the user.",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @GetMapping("/{id}")
    public OrderResponseDto getOrder(@Parameter(description = "ID of the order to be obtained.")
                                         @PathVariable Long id,
                                     @Parameter(hidden = true)
                                         @RequestHeader("X-User-UUID") String userUUID) throws OrderNotFoundException {
        return orderService.getOrder(id, userUUID);
    }
}
