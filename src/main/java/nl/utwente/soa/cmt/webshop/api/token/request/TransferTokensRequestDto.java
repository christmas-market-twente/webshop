package nl.utwente.soa.cmt.webshop.api.token.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferTokensRequestDto {

    private List<TransactionRequestDto> transactions;

    public TransferTokensRequestDto(List<TransactionRequestDto> transactions) {
        this.transactions = transactions;
    }

    public TransferTokensRequestDto() {}
}