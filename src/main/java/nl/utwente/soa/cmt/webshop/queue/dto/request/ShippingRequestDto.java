package nl.utwente.soa.cmt.webshop.queue.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ShippingRequestDto {

    private Long orderId;

    private Long requestedAt;

    private List<ProductShippingRequestDto> products;

    public ShippingRequestDto(Long orderId, Long requestedAt, List<ProductShippingRequestDto> products) {
        this.orderId = orderId;
        this.requestedAt = requestedAt;
        this.products = products;
    }

    public ShippingRequestDto() {}
}
