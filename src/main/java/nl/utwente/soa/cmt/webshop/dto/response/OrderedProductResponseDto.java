package nl.utwente.soa.cmt.webshop.dto.response;

import lombok.Getter;
import lombok.Setter;
import nl.utwente.soa.cmt.webshop.model.OrderedProduct;

@Getter
@Setter
public class OrderedProductResponseDto {

    private Long id;

    private Long productId;

    private Integer amount;

    private Integer unitPrice;

    public static OrderedProductResponseDto from(OrderedProduct orderedProduct) {
        OrderedProductResponseDto responseDto = new OrderedProductResponseDto();
        responseDto.setId(orderedProduct.getId());
        responseDto.setProductId(orderedProduct.getProductId());
        responseDto.setAmount(orderedProduct.getAmount());
        responseDto.setUnitPrice(orderedProduct.getUnitPrice());
        return responseDto;
    }
}
