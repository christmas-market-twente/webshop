package nl.utwente.soa.cmt.webshop.service;

import nl.utwente.soa.cmt.webshop.repository.OrderRepository;
import nl.utwente.soa.cmt.webshop.dto.response.OrderResponseDto;
import nl.utwente.soa.cmt.webshop.exception.OrderNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    public List<OrderResponseDto> getAllOrders(String account) {
        return orderRepository
            .findAllByAccountOrderByCreatedAtDesc(account)
            .stream()
            .map(OrderResponseDto::from)
            .collect(Collectors.toList());
    }

    public OrderResponseDto getOrder(Long id, String account) throws OrderNotFoundException {
        return orderRepository.findByIdAndAccount(id, account).map(OrderResponseDto::from).orElseThrow(OrderNotFoundException::new);
    }
}
