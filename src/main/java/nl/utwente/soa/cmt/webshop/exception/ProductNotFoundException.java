package nl.utwente.soa.cmt.webshop.exception;

import org.springframework.http.HttpStatus;

public class ProductNotFoundException extends InventoryServiceException {

    public ProductNotFoundException() {
        super(HttpStatus.NOT_FOUND, "Product not found");
    }
}
