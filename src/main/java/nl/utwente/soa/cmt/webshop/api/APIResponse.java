package nl.utwente.soa.cmt.webshop.api;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class APIResponse<T> {

    private boolean success;

    private T data;
}
