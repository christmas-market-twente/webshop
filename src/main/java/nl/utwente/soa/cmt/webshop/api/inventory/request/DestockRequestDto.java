package nl.utwente.soa.cmt.webshop.api.inventory.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class DestockRequestDto {

    private List<ProductDestockRequestDto> products;

    public DestockRequestDto(List<ProductDestockRequestDto> products) {
        this.products = products;
    }

    public DestockRequestDto() {}
}
