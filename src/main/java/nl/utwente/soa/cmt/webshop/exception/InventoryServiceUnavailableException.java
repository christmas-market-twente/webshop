package nl.utwente.soa.cmt.webshop.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class InventoryServiceUnavailableException extends InventoryServiceException {

    public InventoryServiceUnavailableException() {
        super(HttpStatus.SERVICE_UNAVAILABLE, "Inventory service unavailable");
    }
}
