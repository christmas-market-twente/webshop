package nl.utwente.soa.cmt.webshop.dto.response;

import lombok.Getter;
import lombok.Setter;
import nl.utwente.soa.cmt.webshop.model.Order;
import nl.utwente.soa.cmt.webshop.model.OrderedProduct;

import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
public class OrderResponseDto {

    private Long id;

    private Long createdAt;

    private Boolean isWebshopPurchase;

    private Long shipmentAcceptedAt;

    private Long shippedAt;

    private Set<OrderedProductResponseDto> products;

    public static OrderResponseDto from(Order order) {
        OrderResponseDto responseDto = new OrderResponseDto();
        responseDto.setId(order.getId());
        responseDto.setCreatedAt(order.getCreatedAt());
        responseDto.setShipmentAcceptedAt(order.getShipmentAcceptedAt());
        responseDto.setShippedAt(order.getShippedAt());
        responseDto.setIsWebshopPurchase(order.getIsWebshopPurchase());
        responseDto.setProducts(
            order.getProducts().stream().map(OrderedProductResponseDto::from).collect(Collectors.toSet())
        );
        return responseDto;
    }
}
